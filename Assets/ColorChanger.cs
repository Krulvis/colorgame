﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;


public class ColorChanger : MonoBehaviour
{
	public GameObject cube;

	public float[] locator = {0, 0, 0};
	String[] colorNames = {"Red", "Green", "Blue"};

	Vector3 getMultiplier(float[] speed)
	{
		var up = Input.GetKey(KeyCode.W) ? 0.001f : 0f;
		var down = Input.GetKey(KeyCode.S) ? -0.001f : 0f;

		var right = Input.GetKey(KeyCode.D) ? 0.001f : 0f;
		var left = Input.GetKey(KeyCode.A) ? -0.001f : 0f;
//        for (int i = 0; i < 2; i++)
//        {
//            
//        }
		speed[0] += up + down;
		speed[1] += left + right;

		for (int i = 0; i < speed.Length; i++)
		{
			speed[i] = fixColorValue(speed[i]);
		}

		return new Vector3((float) speed[0], (float) speed[1], (float) speed[2]);
	}

	float fixColorValue(float value)
	{
		return value < 0.0 ? 0.0f : value > 1.0 ? 1.0f : value;
	}

	// Start is called before the first frame update
	void Start()
	{
		for (int i = 0; i < 1000; i++)
		{
			var color = new Color();
			var position = new Vector3(Random.Range(-50, 50), 0, Random.Range(-50, 50));
			for (int c = 0; c < 3; c++)
			{
				color[c] = (float) (Random.Range(0, 100) / 100.0);
			}

			var newCube = Instantiate(cube, position, Quaternion.Euler(0, 0, 0));
			newCube.GetComponent<MeshRenderer>().material.color = color;
		}
	}

	void checkTransparancy(GameObject gameObject, Vector3 locator)
	{
		var material = gameObject.GetComponent<MeshRenderer>().material;
		var color = material.color;
		var colors = new Vector3(color.r, color.g, color.b);
		var distance = Vector3.Distance(colors, locator);
		color.a = distance < 0.5 ? 0 : 1;

		gameObject.GetComponent<Renderer>().material.color = color;
	}


	// Update is called once per frame
	void Update()
	{
		var gameObjects = FindObjectsOfType<GameObject>();
		foreach (GameObject gameObject in gameObjects)
		{
			if (gameObject.transform.GetComponent<MeshRenderer>())
			{
				checkTransparancy(gameObject, getMultiplier(locator));
			}
		}
	}
}