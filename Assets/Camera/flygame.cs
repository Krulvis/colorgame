﻿using UnityEngine;

namespace Camera
{
	public class flygame : MonoBehaviour
	{
		// Start is called before the first frame update
		void Start()
		{
//        Wobble.shader = multiply;
		}

		/*
		Writen by Windexglow 11-13-10.  Use it, edit it, steal it I don't care.  
		Simple flycam I made, since I couldn't find any others made public.  
		Made simple to use (drag and drop, done) for regular keyboard layout  
		wasd : basic movement
		shift : Makes camera accelerate
		space : Moves camera on X and Z axis only.  So camera doesn't gain any height*/


		public float mainSpeed = 100.0f; //regular speed
		public float shiftAdd = 250.0f; //multiplied by how long shift is held.  Basically running
		public float maxShift = 1000.0f; //Maximum speed when holdin gshift
		public float camSens = 0.25f; //How sensitive it with mouse

		private Vector3
			lastMouse = new Vector3(255, 255, 255); //kind of in the middle of the screen, rather than at the top (play)

		private float totalRun = 1.0f;

		void Update()
		{
			lastMouse = Input.mousePosition - lastMouse;
			lastMouse = new Vector3(-lastMouse.y * camSens, lastMouse.x * camSens, 0);
			var transform = GetComponent<Transform>();
			var position = transform.position;
			var eulerAngles = transform.eulerAngles;
			lastMouse = new Vector3(eulerAngles.x + lastMouse.x, eulerAngles.y + lastMouse.y, 0);
			transform.eulerAngles = lastMouse;
			lastMouse = Input.mousePosition;
			//Mouse  camera angle done.  

			//Keyboard commands
			var f = 0.0f;
			var p = GetBaseInput();
			if (Input.GetKey(KeyCode.LeftShift))
			{
				totalRun += Time.deltaTime;
				p = p * totalRun * shiftAdd;
				p.x = Mathf.Clamp(p.x, -maxShift, maxShift);
				p.y = Mathf.Clamp(p.y, -maxShift, maxShift);
				p.z = Mathf.Clamp(p.z, -maxShift, maxShift);
			}
			else
			{
				totalRun = Mathf.Clamp(totalRun * 0.5f, 1f, 1000f);
				p = p * mainSpeed;
			}

			p = p * Time.deltaTime;
			if (Input.GetKey(KeyCode.Space))
			{
				//If player wants to move on X and Z axis only
				f = position.y;
				transform.Translate(p);
				position.y = f;
			}
			else
			{
				transform.Translate(p);
			}
		}

		private Vector3 GetBaseInput()
		{
			//returns the basic values, if it's 0 than it's not active.
			Vector3 velo = new Vector3();
			if (Input.GetKey(KeyCode.W))
			{
				velo += new Vector3(0, 0, 1);
			}

			if (Input.GetKey(KeyCode.S))
			{
				velo += new Vector3(0, 0, -1);
			}

			if (Input.GetKey(KeyCode.A))
			{
				velo += new Vector3(-1, 0, 0);
			}

			if (Input.GetKey(KeyCode.D))
			{
				velo += new Vector3(1, 0, 0);
			}

			return velo;
		}
	}
}