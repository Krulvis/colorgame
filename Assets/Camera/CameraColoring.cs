﻿using System;
using UnityEngine;

namespace Camera
{
	public class CameraColoring : MonoBehaviour
	{
		public Material material;
		public GameObject target;
		private Color color;
		private Vector3 position;
		private float distance;
		private Texture2D guiTexture;
		private GUIStyle guiStyle = new GUIStyle();
		[Header("Shaders"), Space] public Shader multiply;

		private void Start()
		{
			material.shader = multiply;
		}

		private void Update()
		{
		}

		private void OnRenderImage(RenderTexture source, RenderTexture destination)
		{
			position = target.GetComponent<Transform>().localPosition;
			var localPosition = transform.localPosition;
			distance = Vector3.Distance(localPosition, position);

			color = new Color(position.x + 0.5f * distance, position.y + 0.5f * distance, position.z + 0.5f * distance);
			material.SetColor("_Color", color);
			Graphics.Blit(source, destination, material);


//        RenderSettings.fogMode = FogMode.Linear;
//        RenderSettings.fogEndDistance = 1;
//        RenderSettings.fogStartDistance = 0;
//        RenderSettings.fogColor = color;
//        RenderSettings.fogDensity = 1000; //(float) Math.Pow(1 - (float) Math.Log(distance), 2);
		}

		public void OnGUI()
		{
			GUI.color = Color.white;
			GUI.Label(new Rect(0f, 0f, 200f, 200f),
				"Red: " + Math.Round(color.r, 2) +
				"\n" + "Green: " + Math.Round(color.g, 2) +
				"\n" + "Blue: " + Math.Round(color.b, 2) +
				"\n" + "X: " + Math.Round(position.x, 2) +
				"\n" + "Y: " + Math.Round(position.y, 2) +
				"\n" + "Z: " + Math.Round(position.z, 2) +
				"\n" + "D: " + Math.Round(distance, 2)
			);
			guiTexture = new Texture2D(1, 1);
			guiTexture.SetPixel(0, 0, color);
			guiTexture.Apply();

			guiStyle.normal.background = guiTexture;
			GUI.Box(new Rect(0f, 200f, 100f, 100f), GUIContent.none, guiStyle);
		}
	}
}